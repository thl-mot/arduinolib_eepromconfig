/*
 * Config.cpp
 *
 *  Created on: 13.01.2018
 *      Author: Thomas Lauerbach
 */

#include <EEPROMConfig.h>

//EEPROMConfig::EEPROMConfig() {
//	configStructSize = 0;
//	byteStorage=0;
//	eepromBaseAddressCRC16 = 0;
//	eepromBaseAddressData = 2;
//	crc=0;
//}
//
//
//EEPROMConfig::~EEPROMConfig() {
//}
//
//bool EEPROMConfig::init(uint8_t * configPtr, uint16_t size, uint16_t baseAddress) {
//	configStructSize = size;
//	byteStorage= configPtr;
//
//	eepromBaseAddressCRC16 = baseAddress;
//	eepromBaseAddressData = baseAddress + 2;
//
//	load();
//
//	if (!valid) {
//		for (size_t i = 0; i < configStructSize; i++) {
//			byteStorage[i] = 0;
//		}
//		crc = 0;
//	}
//	return valid;
//}
//
//void EEPROMConfig::save() {
//	uint8_t * crcStorage = (uint8_t *) &crc;
//
//	EEPROM.begin(2 + configStructSize);
//
//	Config_CRC16 crcCalculator;
//	crcCalculator.reset();
//	for (int i = 0; i < configStructSize; i++) {
//		crcCalculator.add(byteStorage[i]);
//		EEPROM.write(eepromBaseAddressData + i, byteStorage[i]);
//	}
//
//	crc = crcCalculator.getCrc();
//	EEPROM.write(eepromBaseAddressCRC16, crcStorage[0]);
//	EEPROM.write(eepromBaseAddressCRC16 + 1, crcStorage[1]);
//
//	EEPROM.commit();
//	EEPROM.end();
//	load();
//}
//
//
//bool EEPROMConfig::isValid() {
//	return valid;
//}
//
//
//void EEPROMConfig::load() {
//	uint8_t * crcStorageRead = (uint8_t *) &crc;
//
//	EEPROM.begin(2 + configStructSize);
//
//	crcStorageRead[0] = EEPROM.read(eepromBaseAddressCRC16);
//	crcStorageRead[1] = EEPROM.read(eepromBaseAddressCRC16 + 1);
//
//	Config_CRC16 crcCalculator;
//	crcCalculator.reset();
//	for (size_t i = 0; i < configStructSize; i++) {
//		byteStorage[i] = EEPROM.read(eepromBaseAddressData + i);
//		crcCalculator.add( byteStorage[i]);
//	}
//
//	valid= (crc==crcCalculator.getCrc());
//	EEPROM.end();
//}
//

bool configLoad( uint8_t * byteStorage, uint16_t configStructSize, uint16_t baseAddress) {
	const uint16_t eepromBaseAddressCRC16 = baseAddress;
	const uint16_t eepromBaseAddressData  = baseAddress + 2;
	uint16_t crc=0;

	uint8_t * crcStorageRead = (uint8_t *) &crc;

	EEPROM.begin(2 + configStructSize);

	crcStorageRead[0] = EEPROM.read(eepromBaseAddressCRC16);
	crcStorageRead[1] = EEPROM.read(eepromBaseAddressCRC16 + 1);

	Config_CRC16 crcCalculator;
	crcCalculator.reset();
	for (size_t i = 0; i < configStructSize; i++) {
		byteStorage[i] = EEPROM.read(eepromBaseAddressData + i);
		crcCalculator.add( byteStorage[i]);
	}

	bool valid= (crc==crcCalculator.getCrc());
	EEPROM.end();
	if (!valid) {
		for (size_t i = 0; i < configStructSize; i++) {
			byteStorage[i] = 0;
		}
	}
	return valid;
}

void configSave( uint8_t * byteStorage, uint16_t configStructSize, uint16_t baseAddress) {
	const uint16_t eepromBaseAddressCRC16 = baseAddress;
	const uint16_t eepromBaseAddressData  = baseAddress + 2;
	uint16_t crc=0;

	uint8_t * crcStorage = (uint8_t *) &crc;

	EEPROM.begin(2 + configStructSize);

	Config_CRC16 crcCalculator;
	crcCalculator.reset();
	for (int i = 0; i < configStructSize; i++) {
		crcCalculator.add(byteStorage[i]);
		EEPROM.write(eepromBaseAddressData + i, byteStorage[i]);
	}

	crc = crcCalculator.getCrc();
	EEPROM.write(eepromBaseAddressCRC16, crcStorage[0]);
	EEPROM.write(eepromBaseAddressCRC16 + 1, crcStorage[1]);

	EEPROM.commit();
	EEPROM.end();
}


