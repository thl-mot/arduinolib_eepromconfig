/*
 * Config.h
 *
 *  Created on: 13.01.2019
 *      Author: Thomas Lauerbach
 */

#ifndef EEPROMConfig_H_
#define EEPROMConfig_H_

#include <Arduino.h>
#include <EEPROM.h>
#include "ConfigCRC16.h"

//class EEPROMConfig {
//private:
//	int configStructSize = 0;
//	int eepromBaseAddressCRC16 = 0;
//	int eepromBaseAddressData = 2;
//	bool valid = false;
//
//	void load();
//public:
//	uint16_t crc;
//	uint8_t * byteStorage;
//
//	EEPROMConfig();
//	virtual ~EEPROMConfig();
//
//	bool init(uint8_t * configPtr, uint16_t size, uint16_t baseAddress=0);
//
//	void save();
//	bool isValid();
//};

	bool configLoad( uint8_t * byteStorage, uint16_t configStructSize, uint16_t baseAddress);
	void configSave( uint8_t * byteStorage, uint16_t configStructSize, uint16_t baseAddress);



#endif /* EEPROMConfig_H_ */
